(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)

(*
* PWD - Print Wording Directory
*
* This program prints the current directory to shell
*)

val args = String.concatWith " " (CommandLine.arguments());

val () = TextIO.print (args ^ "\n");

