(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)

(*
* PWD - Print Wording Directory
*
* This program prints the current directory to shell
*)

fun getDir () = OS.FileSys.getDir();

val () = TextIO.print (getDir() ^ "\n");

