(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)


(*
* Sort.
*
* This program uses a Red-Black tree to sort lines read from stdin and sends
* returns the sorted text via stdout.
*)

(*
* Red-Black tree implementation use here is adapted from CS312 lecture notes
* from Cornell:
*
* http://www.cs.cornell.edu/courses/cs312/2004fa/lectures/lecture11.htm
*)
datatype Color = Red | Black;
datatype 'a rbtree = Leaf
                   | Node of Color * 'a * 'a rbtree * 'a rbtree;


structure RedBlack =
struct
    val cmp = String.compare

    val empty : string rbtree = Leaf

    fun insert Leaf v = Node (Black, v, Leaf, Leaf)
      | insert (Node (c, v, l, r)) vn = Leaf;

    fun find Leaf _ = false
      | find (Node (_, nv, l, r)) v =
            if v = nv then true
            else if v < nv then find l v
            else find r v

    fun insert tr value =
    let fun makeBlack Leaf = Leaf
          | makeBlack (Node (_, v, l, r)) = Node (Black, v, l, r)

        fun rotate (x:string) (y:string) (z:string) (a:string rbtree) (b:string rbtree) (c:string rbtree) (d:string rbtree) : string rbtree=
             Node (Red, y, (Node (Black, x, a, b)),
                           (Node (Black, z, c, d)))

        (* Balance a subtree t so that insertion doesn't violate tree
        * invariants. If no invariants will be violated, then do nothing. *)
        fun balance t : string rbtree =
            case t of
                 Node (Black, z, (Node (Red, y, Node(Red, x, a, b), c)), d) => rotate x y z a b c d
               | Node (Black, z, Node (Red, x, a, Node (Red, y, b, c)), d) => rotate x y z a b c d
               | Node (Black, x, a, Node (Red, z, Node (Red, y, b, c), d)) => rotate x y z a b c d
               | Node (Black, x, a, Node (Red, y, b, Node (Red, z, c, d))) => rotate x y z a b c d
               | _ => t


        (* Walk through tree, balancing nodes as necessary. *)
        fun walk Leaf = Node (Red, value, Leaf, Leaf)
          | walk (t as (Node (c, v, l, r))) =
                (case cmp (v, value) of
                    (*LESS    => balance (Node (c, v, walk l, r))
                    | EQUAL   => t
                    | GREATER => balance (Node (c, v, l, walk r)))*)

                    (* We deviate from the sample code here so that we preserve
                    * duplicates of the original input. The original version of
                    * this case statement is shown above. *)
                      GREATER => balance (Node (c, v, l, walk r))
                    | _    => balance (Node (c, v, walk l, r)))

    in makeBlack (walk tr)
    end
end;


(* Create new redblack tree, and add lines from stdin to the tree. *)
val t = ref (RedBlack.empty);
while not (TextIO.endOfStream TextIO.stdIn)
do (let val line = TextIO.inputLine TextIO.stdIn
        val trimmed = (case line of
             NONE => raise Fail "couldn't read line"
           | SOME l => String.substring (l, 0, String.size (l) - 1))
    in t := RedBlack.insert (!t) trimmed end);


fun inorder Leaf = []
  | inorder (Node (_, v, l, r)) = inorder l @ [v] @ inorder r;

val elements = rev (inorder (!t));


val appendNewlineAndPrint = fn x => TextIO.print (x ^ "\n");
app appendNewlineAndPrint elements;



