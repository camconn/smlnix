(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)

(*
* TOUCH - Change the modification time of a file.
*
* This program behaves similarly to the Unix program `touch`. It will either (1)
* change the modification time of the file(s) specified or (2) create a new file
* if the file specified does not exist.
*
*)

(*
* Here, first make sure the file exists by opening it as a file to append to.
* Then, we set the modified time now. Doing the append and THEN doing the time
* modification make sure that the setTime operation can't fail.
*)
fun updateFileTime name =
let val os = TextIO.openAppend name
    val () = TextIO.closeOut os
in OS.FileSys.setTime (name, NONE) end;

val () = app (updateFileTime) (CommandLine.arguments ());

