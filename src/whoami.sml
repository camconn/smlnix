(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)

(*
* WHOAMI - Print current username.
*
* Print the username of the current user.
*)

val uid = Posix.ProcEnv.getlogin ();

val () = TextIO.print(uid ^ "\n");


