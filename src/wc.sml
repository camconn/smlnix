(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)


(*
* WC - Word Count
*
* This program counts the lines, words, and bytes passed to it via stdin.
* Results are reports via stdout. We don't have full parity with the
* functionality of the actual `wc` utility, but this primitive version is good
* enough.
*)

fun processChar (lines, words, bytes, inWord, ch) =
let val inWord' = Char.isAlphaNum ch
    val words' = if inWord' = inWord then words  (* Unchanged *)
                 (* If we weren't in a word and now we are then go ahead and
                 * add another word to our counter*)
                 else if (not inWord) andalso inWord' then words+1
                 else words
    val lines' = if ch = #"\n" then lines+1 else lines
    val bytes' = bytes + 1
in (lines', words', bytes', inWord')
end;


(* This is the imperative part of the program. *)
val ins = TextIO.stdIn;

(* Initially, our cumulative counts for lines, words, and bytes are zero.
* Moreover, we also consider ourselves /not/ to be in a word. *)
val lines = ref 0
and words = ref 0
and bytes = ref 0
and inWord = ref false;

(* While we are stilling getting stuff from stdin, go ahead and count it. *)
while not (TextIO.endOfStream ins)
do (let val c = TextIO.input1 (ins)
    in (case c of
            SOME ch => (let val results = processChar (!lines, !words, !bytes, !inWord, ch)
                        in (lines := (#1 results);
                            words := (#2 results);
                            bytes := (#3 results);
                            inWord := (#4 results))
                        end)
          | NONE => raise Fail "We shouldn't be able to get here at all.") end);


val () = TextIO.print (
        Int.toString(!lines) ^ " " ^
        Int.toString(!words) ^ " " ^
        Int.toString(!bytes) ^ "\n");


