(*
* SMLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)


(*
* Head - Display first few lines of a file.
*
* This program counts the lines, words, and bytes passed to it via stdin.
* Results are reports via stdout. We don't have full parity with the
* functionality of the actual `wc` utility, but this primitive version is good
* enough.
*)


fun getLines 0 _ : string list = []
  | getLines n src =
    let val next = TextIO.inputLine src
    in (case next of
            NONE        => []
          | (SOME line) => line :: (getLines (n-1) src))
    end;


(* TODO: Alert that we have not been able to open the bad file. *)
fun getTargets []      = []
  | getTargets (x::xs) = (
        (TextIO.openIn x) :: getTargets xs
  ) handle (IO.Io _) => getTargets xs;



(* Number of lines to display off the top. *)
val numLines = 10;

val targets = getTargets (CommandLine.arguments());

val _ =
    if length targets = 0
    then raise Fail "nothing to read"
    else (let val lines = map (getLines numLines) targets
          in map (app (TextIO.print)) lines
          end);

