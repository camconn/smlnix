(*
* MLnix - Unix utilties written in Standard ML.
*
* © 2019 Cameron Conn
*)

(*
* LS - List files
*
* This program lists the files in the given directory. By default, the program
* uses the current directory as the directory to list. For accepted arguments,
* we only accept the first argument.
*)

exception BlankName of string;

fun getDirFiles "" = raise BlankName "Blank name passed to getDirFiles"
  | getDirFiles name =
    let fun printFiles d =
        case OS.FileSys.readDir d of
              SOME filename => filename :: printFiles d
            | NONE => []
    in printFiles (OS.FileSys.openDir (name)) end;


fun printDirFiles d = TextIO.print(String.concatWith "\n" (getDirFiles d) ^ "\n");


(* Where we do our side effects. *)
fun findTargetDirs [] = [OS.FileSys.getDir()]
  | findTargetDirs ns = ns;

val targets = findTargetDirs (CommandLine.arguments ());

val () = app printDirFiles targets;

