# SMLnix
Unix utilities implemented using Standard ML

This project is available at https://gitlab.com/camconn/smlnix

# Requirements
The only requirement for this project is an SML97 compliant environment with
the Basis library.

SMLnix is developed against MLton and SMLNJ, but the project should work with
all SML environments that implement the basis library.

# Usage
You may run these programs either as a binary, or as an interpreted script.
Some examples are listed below:

## sort - Sort lines
`sml src/sort.sml < data/test.txt`

## wc - Word count
`sml src/wc.sml < data/test.txt`

# Features/Utilities
This project implements a number of Unix utilities in Standard ML. No attempt
is made to reimplement every single binary from GNU coreutils in SML.

- [ ] cat
- [ ] du
- [x] echo - print text to stdout
- [ ] head
- [x] ls - list files
- [ ] mkdir
- [ ] mv
- [x] pwd - print working directory
- [ ] rm
- [ ] rmdir
- [x] sort - sort lines
- [ ] tail
- [x] touch - change file modification times
- [x] wc - count lines, words, and bytes
- [x] whoami - print current username

# Motivation
I wrote this project to cement my knowledge of Standard ML and to learn how to
write real-world applications using functional programming.

Feel free to use this code to teach yourself or teach others, as long as you
abide by the project license.

# License
You may freely redistribute and use the code of this project under the terms
of the GNU Public License, Version 3 or any later version. A copy of this
license is located in `LICENSE.txt`.

